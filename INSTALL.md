Install driver for Wificard: TP-LINK TL-WN822N V4
Driver used:rtl8192eu

1)Install git:
 sudo apt-get update
 sudo apt-get install git

2)Copy a file of the driver into your computer:
git clone https://github.com/jeremyb31/rtl8192eu-linux-driver

3)Navigate to the directory of the driver
  cd rtl8192eu-linux-driver

4)Build the driver software and install:
  make
  sudo make install

5) Restart the computer so the driver start working:
  reboot

6)Make a new wifi connection with
  Realtek 802.11n WLAN Adapter

Note: If your the wifi adapter connection doesnt show up after rebooting, try turning off the secure boot option to enable the third party driver to work 

